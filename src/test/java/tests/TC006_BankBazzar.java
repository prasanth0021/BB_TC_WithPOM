package tests;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import bb_pages.BB_page1;
import wdMethods.ProjectMethods;

public class TC006_BankBazzar extends ProjectMethods{
	@BeforeClass
	public void setdata()
	{
		testCaseName ="TC_Bankbazar";
		testCaseDescription = "  ";
		author = " prasanth ";
		category = "smoke";
		dataSheetName="TC006";
	}
	@Test(dataProvider = "fetchData" )
	public void bankbazzarrun(String monthyear , String day , String bday , String fname) throws InterruptedException
	{
		new BB_page1()
		.mouseoveroninvesment()
		.clickmutualfunds()
		.clicksearchmutualfunds()
		.clickyear()
		.clickmonth(monthyear)
		.clickdate(day)
		.verifybday(bday)
		.clickcontinue()
		.selectincome()
		.clickcon()
		.selectbank("Axis")
		.firstname(fname)
		.clickviewmutualfund()
		.schemename();
		
		
	}
}

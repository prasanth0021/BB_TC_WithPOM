package tests;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;

//import org.junit.Test;

import org.testng.annotations.Test;

import pages.MyHomePage;
import wdMethods.ProjectMethods;

public class TC005_MergeLead extends ProjectMethods {
	
	@BeforeClass
	public void setData() {
		testCaseName = "TC005_MergeLead";
		testCaseDescription ="Merge two leads";
		category = "Smoke";
		author= "Babu";
		dataSheetName="TC005";
	}
	@Test(dataProvider="fetchData")
	public  void mergeLead(String fname1, String fname2 ,String findname) throws InterruptedException    {
		new MyHomePage()
		.clickLeads()
		.clickMergeLead()
		.clickFirstImg()
		.typeFirstName(fname1)
		.clickFindLead()
		.clickFirstLead()
		.clickSecondImg()
		.typeFirstName(fname2)
		.clickFindLead()
		.clickFirstLead()
		.clickMerge()
		.clickFindLead()
		.typeFirstName(findname)
		.clickFindLead();
		
	
		
		
	}

}

package steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import wdMethods.SeMethods;

public class Hooks extends SeMethods{
	@Before
	public void beforecucumber(Scenario sc)
	{
		//System.out.println("Scenario name :" +sc.getName());
		//System.out.println("Scenario id : "+sc.getId());
		startResult();
		testCaseName = sc.getName();
		testCaseDescription =sc.getId();
		category = "Smoke";
		author= "prasanth";
		startTestCase();
		startApp("chrome", "http://leaftaps.com/opentaps");

	}
	@After
	public void afterscenario(Scenario sc)
	{
		//System.out.println("status :"+sc.getStatus());
		closeAllBrowsers();
		stopResult();
	}
}

package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import wdMethods.ProjectMethods;

public class ViewLeadsPage extends ProjectMethods{


	/*@CacheLookup
	@FindBy(linkText = "Edit")
	WebElement eleEdit;
	
	@CacheLookup
	@FindBy(linkText = "Find Leads")
	WebElement elefindlead;*/
	
	/*@CacheLookup
	@FindBy(linkText = "Delete")
	WebElement eledelete;*/
	
	/*@CacheLookup
	@FindBy(linkText = "Duplicate Lead")
	WebElement eleduplicate;*/
	
	public EditLeadPage clickEdit()
	{
		WebElement eleEdit = locateElement("linktext", "Edit");
		click(eleEdit);
		return new EditLeadPage();
	
}
	
	public FindLeadPage clickFindLead()
	{
		WebElement elefindlead = locateElement("linktext" , "Find Leads");
		click(elefindlead);
		return new FindLeadPage();
	}
	
	public MyLeadsPage clickDelete()
	{
		WebElement eledelete = locateElement("linktext", "Delete");
		click(eledelete);
		return new MyLeadsPage();
	}
	
	public DuplicateLeadPage clickDuplicate()
	{
		WebElement eleduplicate = locateElement("linktext", "Duplicate Lead");
		click(eleduplicate);		
		return new DuplicateLeadPage();
	}
}











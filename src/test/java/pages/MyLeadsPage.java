package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class MyLeadsPage extends ProjectMethods{
	/*@CacheLookup
	@FindBy(linkText = "Create Lead")
	WebElement eleCreateLead;*/
	
	/*@CacheLookup
	@FindBy(linkText = "Find Leads")
	WebElement elefindleads;*/
	
	/*@CacheLookup
	@FindBy(linkText = "Merge Leads")
	WebElement elemergelead;*/
	@And("click create lead")
	public CreateLeadPage clickCreateLead() {
		WebElement eleCreateLead = locateElement("linktext", "Create Lead");
		click(eleCreateLead);
		return new CreateLeadPage();
		
		
	}
	
	
	public FindLeadPage clickFindLeads()
	{
		WebElement elefindleads = locateElement("linktext", "Find Leads");
		click(elefindleads);
		return new FindLeadPage();
	}
	
	public MergeLeadPage clickMergeLead()
{
		WebElement elemergelead = locateElement("linktext", "Merge Leads");
		click(elemergelead);
		return new MergeLeadPage();
		
	
}
	
}










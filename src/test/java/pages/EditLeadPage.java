package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import wdMethods.ProjectMethods;

public class EditLeadPage extends ProjectMethods{
	/*@CacheLookup
	@FindBy(className ="updateLeadForm_companyName")
	WebElement eleCreateLead;*/
	public EditLeadPage typeCompanyName(String data) {
		WebElement elecompanyname = locateElement("id", "updateLeadForm_companyName");
		elecompanyname.clear();
		type(elecompanyname, data);
		return this;
	}
	
	
	public EditLeadPage clickUpdate() {
		WebElement eleupdate = locateElement("xpath", "//input[@class='smallSubmit']");
		click(eleupdate);
		return this;
	}
	
}










package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import wdMethods.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{
/*@CacheLookup
@FindBy(id = "createLeadForm_companyName")
WebElement eleCompanyName;

@CacheLookup
@FindBy(id = "createLeadForm_firstName")
WebElement eleFirstName;

@CacheLookup
@FindBy(className = "smallSubmit")
WebElement eleLastName;

@CacheLookup
@FindBy(id = "createLeadForm_lastName")
WebElement eleCreateLead;*/
	@And("enter the company name as (.*)")
	public CreateLeadPage typeCompanyName(String data) {
		WebElement eleCompanyName = locateElement("id", "createLeadForm_companyName");
		type(eleCompanyName, data);
		return this;
	}
	@And("enter the first name as (.*)")
	public CreateLeadPage typeFirstName	(String data) {
		WebElement eleFirstName = locateElement("id", "createLeadForm_firstName");
		type(eleFirstName, data);
		return this;
	}
	@And("enter the last name as (.*)")
	public CreateLeadPage typeLastName	(String data) {
		WebElement eleLastName = locateElement("id", "createLeadForm_lastName");
		type(eleLastName, data);
		return this;
	}
	@And("click the create lead")
	public CreateLeadPage clickCreateLead() {
		WebElement eleCreateLead= locateElement("class", "smallSubmit");
		click(eleCreateLead);
		return this; 
	}
	@Then("verify the lead")
	public void verify()
	{
		System.out.println("verified");
	}
	
}










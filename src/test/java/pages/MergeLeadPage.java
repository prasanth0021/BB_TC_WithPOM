package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import wdMethods.ProjectMethods;

public class MergeLeadPage extends ProjectMethods{
/*@CacheLookup
@FindBy(xpath = "(//table[@id='widget_ComboBox_partyIdFrom']/following::img)[1]")
WebElement elefirstimg;

@CacheLookup
@FindBy(xpath = "(//table[@id='widget_ComboBox_partyIdFrom']/following::img)[2]")
WebElement eleSecondimg;

@CacheLookup
@FindBy(linkText = "Merge")
WebElement elemerge;*/
	public SwitchWindowMergePage clickFirstImg() {
		WebElement elefirstimg = locateElement("xpath", "(//table[@id='widget_ComboBox_partyIdFrom']/following::img)[1]");
		click(elefirstimg);
		switchToWindow(1);
		return new SwitchWindowMergePage();
	}
	
	
	public SwitchWindowMergePage clickSecondImg() {
		WebElement eleSecondimg = locateElement("xpath", "(//table[@id='widget_ComboBox_partyIdFrom']/following::img)[2]");
		click(eleSecondimg);
		switchToWindow(1);
		return new SwitchWindowMergePage(); 
	
	}
	
	public ViewLeadsPage clickMerge() 
	{
		WebElement elemerge = locateElement("linktext", "Merge");
		click(elemerge);
		acceptAlert();
		return new ViewLeadsPage();
	}
}










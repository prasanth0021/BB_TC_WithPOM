package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import wdMethods.ProjectMethods;

public class DuplicateLeadPage extends ProjectMethods{


/*@CacheLookup
@FindBy(id = "createLeadForm_lastName")
WebElement eleCreateLead;*/
	
	
	public DuplicateLeadPage clickCreateLead(String data) {
		verifyTitle(data);
		WebElement eleCreateLead= locateElement("class", "smallSubmit");
		click(eleCreateLead);
		return this;
	}
	
}










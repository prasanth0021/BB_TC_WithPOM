package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import wdMethods.ProjectMethods;

public class FindLeadPage extends ProjectMethods{
/*@CacheLookup
@FindBy(xpath = "(//input[@name='firstName'])[3]")
WebElement elefirstname;
*/
/*@CacheLookup
@FindBy("xpath", "//button[text()='Find Leads']")
WebElement elefindlead;
*/
/*@CacheLookup
@FindBy(xpath = "(//td[@class='x-grid3-col x-grid3-cell x-grid3-td-firstName ']//a)[1]")
WebElement elefirstmatch;*/
	public FindLeadPage typeFirstName(String data) {
		WebElement elefirstname = locateElement("xpath", "(//input[@name='firstName'])[3]");
		type(elefirstname, data);
		return this;
	}
	
	
	public FindLeadPage clickFindLead() {
		WebElement elefindlead = locateElement("xpath", "//button[text()='Find Leads']");
		click(elefindlead);
		return this; 
	
	}
	
	public ViewLeadsPage clickFirstMatch() throws InterruptedException
	{
		Thread.sleep(3000);
		WebElement elefirstmatch = locateElement("xpath", "(//td[@class='x-grid3-col x-grid3-cell x-grid3-td-firstName ']//a)[1]");
		click(elefirstmatch);
		return new ViewLeadsPage();
	}
}










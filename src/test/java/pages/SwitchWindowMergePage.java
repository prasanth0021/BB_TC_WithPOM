package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;

import wdMethods.ProjectMethods;

public class SwitchWindowMergePage extends ProjectMethods{
	/*@CacheLookup
	@FindBy(xpath ="(//div[@class='x-form-element']/input)[2]")
	WebElement eleFirstName;
	
	@CacheLookup
	@FindBy(xpath ="(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]")
	WebElement eleFirstLead;*/
	public SwitchWindowMergePage typeFirstName(String data) {
		
		WebElement eleFirstName = locateElement("xpath", "(//div[@class='x-form-element']/input)[2]");
		type(eleFirstName, data);
		return this;
	}
	
	public SwitchWindowMergePage clickFindLead()
	{
		WebElement elefindlead = locateElement("xpath", "//button[text()='Find Leads']");
		click(elefindlead);
		return this;
	}
	
	public MergeLeadPage clickFirstLead() throws InterruptedException
	{
		Thread.sleep(3000);
		WebElement eleFirstLead = locateElement("xpath", "(//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a)[1]");
	clickWithNoSnap(eleFirstLead);
	switchToWindow(0);
	return new MergeLeadPage();
	}
	

	
}

;








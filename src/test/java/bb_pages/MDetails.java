package bb_pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class MDetails extends ProjectMethods{
public MDetails firstname(String fname)
{
	WebElement e1 = locateElement("xpath", "//input[@name='firstName']");
	type(e1, fname);
	return this;
}

public Scheme clickviewmutualfund()
{
	WebElement e2 = locateElement("linktext", "View Mutual Funds");
	click(e2);
	return new Scheme();

}

}

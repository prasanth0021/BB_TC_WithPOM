package bb_pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import wdMethods.ProjectMethods;

public class AnualIncome extends ProjectMethods{


	public AnualIncome selectincome()
	{
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Actions act = new Actions(driver);
		WebElement e1 = locateElement("xpath", "//div[@class='rangeslider__handle']");
		String text="";
		do {
			act.dragAndDropBy(e1, 5, 0).perform();
			WebElement salary = locateElement("xpath", "//div[@class='rangeslider__handle']/div");
			text = salary.getText();
		} while (!text.equals("5,00,000"));
		/*	WebElement e1 = locateElement("xpath", "//span[text()='2,50,000']");
		click(e1);*/
		return this;
	}
	public Bankopt clickcon()
	{
		WebElement e1 = locateElement("linktext", "Continue");
		click(e1);
		return new Bankopt();
	}
}

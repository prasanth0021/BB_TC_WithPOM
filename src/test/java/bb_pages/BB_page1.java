package bb_pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import wdMethods.ProjectMethods;

public class BB_page1 extends ProjectMethods{
public BB_page1 mouseoveroninvesment()
{
	Actions builder = new Actions(driver);
	builder.moveToElement(driver.findElementByLinkText("INVESTMENTS")).perform();
	return this;
}
public MutualFund clickmutualfunds()
{
	WebElement e1 = locateElement("linktext", "Mutual Funds");
	click(e1);
	try {
		Thread.sleep(3000);
		switchToWindow(1);
	} catch (Exception e) {
		// TODO Auto-generated catch block
		switchToWindow(0);
	}
	return new MutualFund();
}
}

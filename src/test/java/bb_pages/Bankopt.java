package bb_pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class Bankopt extends ProjectMethods {
public MDetails selectbank(String bank)
{
	WebElement e1 = locateElement("xpath", "//label[@class='labelContainer']/span[text()='"+bank+"']");
	click(e1);
	return new MDetails();
}
}

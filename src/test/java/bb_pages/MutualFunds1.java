package bb_pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import wdMethods.ProjectMethods;

public class MutualFunds1 extends ProjectMethods {

	public MutualFunds1 clickyear() throws InterruptedException
	{
		Actions dg = new Actions(driver);
		Thread.sleep(3000);
		WebElement e1 = locateElement("xpath", "//div[@class='rangeslider__handle']");
		dg.dragAndDropBy(e1, 23, 0).perform();
		/*WebElement e1 = locateElement("xpath", "//div[text()='"+year+"']");*/
		/*click(e1);*/
		return this;
		
	}
	public MutualFunds1 clickmonth(String monthYear)
	{
		
		WebElement e2 = locateElement("linktext", monthYear); //ex
		click(e2);
		return this;
	}
	public MutualFunds1 clickdate(String day)
	{
		WebElement e3 = locateElement("xpath", "//div[@aria-label='day-"+day+"']");
		click(e3);
		return this;
	}
	public MutualFunds1 verifybday(String bday)
	{
		WebElement e5 = locateElement("xpath", "//span[@class='Calendar_highlight_xftqk']");
		verifyPartialText(e5, bday);
		return this;
	}
	public AnualIncome clickcontinue()
	{
		WebElement e4 = locateElement("linktext", "Continue");
		click(e4);
		return new AnualIncome();
	}
	
	
}

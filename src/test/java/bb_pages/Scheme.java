package bb_pages;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import wdMethods.ProjectMethods;

public class Scheme extends ProjectMethods{
public Scheme schemename()
{
	WebDriverWait wait = new WebDriverWait(driver, 30);
	wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//img[@alt='loading']")));
	//wait.until(ExpectedConditions.invisibilityOf((WebElement) By.xpath("//img[@alt='loading']")));
	
	List<WebElement> schemeName = locateElements("class", "js-offer-name");
	List<WebElement> schemeRate = locateElements("xpath", "//div[@class='offer-section-column col-same-height col-middle investment-amount']/span[@class='js-title']");
	Map<String, String> map = new LinkedHashMap<String, String>();
	
	for (int i = 0; i <schemeName.size(); i++) {
		map.put(schemeName.get(i).getText(), schemeRate.get(i).getText());
	}
	
	for (Entry<String, String> schemes : map.entrySet()) {
		System.out.println("Scheme Name: "+schemes.getKey());
		System.out.println("Scheme Rate: "+schemes.getValue());
		
		
	}
	
	
	
	
	/*List<WebElement> allSchemes = driver.findElementsByClassName("js-offer-name");
	for (WebElement eachScheme : allSchemes) {
		System.out.println(eachScheme.getText());
		WebElement eleAmount = driver.findElementByXPath("//span[contains(text(),'"+eachScheme.getText()+"')]/following::span[@class='fui-rupee bb-rupee-xs']/..");
		System.out.println(eleAmount.getText());
	}*/
	return this;
}
}
